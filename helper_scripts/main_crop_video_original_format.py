# Crops a video around the face and exports it into a series of png images called 0.png -> N.png so it can be trained
#  by original FOM.
import numpy as np
import cv2
from pathlib import Path
from argparse import ArgumentParser
from media_processing_lib.video import MPLVideo, video_read
from media_processing_lib.image import image_resize, image_write
from vke.face import vidGetFaceBbox, vidGetFaceKeypoints
from nwutils.bbox import squareBbox, smoothBbox
from nwutils.kps import rescaleKps
from tqdm import trange

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--video_path", required=True)
    parser.add_argument("--export_dir", required=True)
    parser.add_argument("--bbox_padding_percent", help="height%%,width%%. Default:10,0.", default="10,0")
    parser.add_argument("--export_images", default=1, type=int)
    parser.add_argument("--export_video", default=1, type=int)
    args = parser.parse_args()
    args.video_path = Path(args.video_path).absolute()
    args.bbox_padding_percent = [float(x) for x in args.bbox_padding_percent.split(",")]
    assert args.video_path.exists(), f"Does not exist: {args.video_path}"
    assert len(args.bbox_padding_percent) == 2
    args.export_images = bool(args.export_images)
    args.export_video = bool(args.export_video)
    assert args.export_images + args.export_video >= 1, "At least one must be set!"

    return args

def faceAndResolutionAdjustedKPs(kps, bbox, resolution):
    x1, x2, y1, y2 = bbox
    # kps are from the original image. We have a bounding box around the face, so subtracting top_left corner should
    #  align the keypoints to the bounding box
    faceFrameKp = kps - [y1, x1]
    # Now, we'll also adjust the face-adjusted keypoints to the new resolution. Suppose the face bbox is (384x280) and
    #  our target resolution is (256x256). We will make a rapport for height (256/384) and width (256/280) and apply
    #  it to each keypoint. Finally, we convert to int, so they are valid coordinates.
    resKps = rescaleKps(faceFrameKp, originalScale=(y2 - y1, x2 - x1), newScale=resolution)
    resKps = np.clip(resKps, [0, 0], [y2 - y1 - 1, x2 - x1 - 1])
    return resKps

def do_export(video: MPLVideo, bbox, kps, exportDir: Path, export_images: bool=True, export_video: bool=True):
    Path(exportDir).mkdir(exist_ok=False, parents=True)
    height, width = 256, 256
    if export_video:
        writer = cv2.VideoWriter(f"{exportDir}/video.mp4", cv2.VideoWriter_fourcc(*"mp4v"), video.fps, (width, height))

    kpPath = f"{exportDir}/faceKPs.npy"
    resultKps = kps * 0
    for i in trange(len(video)):
        frame = video[i]
        x1, x2, y1, y2 = bbox[i]
        face_frame = frame[y1:y2, x1:x2]
        resized_face_frame = image_resize(face_frame, height=height, width=width)
        faceKp = faceAndResolutionAdjustedKPs(kps[i], bbox[i], (height, width))
        resultKps[i] = faceKp

        if export_images:
            out_png_path = f"{exportDir}/{i}.png"
            image_write(resized_face_frame, out_png_path)
        if export_video:
            writer.write(resized_face_frame[..., ::-1])
    np.save(kpPath, resultKps)

def getFaceBbox(video, paddingPercent):
    def vidBboxAvgRatio(vidBbox):
        x1, x2, y1, y2 = vidBbox.T
        return (x2 - x1).mean(), (y2 - y1).mean()

    def padBbox(video, vidBbox, percent):
        H, W = video.shape[1 : 3]
        yPercent, xPercent = percent
        # Nx4 => 4xN
        vidBbox = vidBbox.T
        x1, x2, y1, y2 = vidBbox
        # Get the left and right padding
        yValue = np.int32(yPercent * (y2 - y1) / 100)
        xValue = np.int32(xPercent * (x2 - x1) / 100)
        # Apply the padding and clip outside of video values
        vidBbox = vidBbox + [-xValue, xValue, -yValue, yValue]
        vidBbox = vidBbox.T
        vidBbox = np.clip(vidBbox, [0, 0, 0, 0], [W, W, H, H])
        # 4xN => Nx4
        return vidBbox

    faceBbox = vidGetFaceBbox(video, faceLib="face_alignment")
    print("[getFaceBbox] Initial bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    faceBbox = smoothBbox(faceBbox)
    print("[getFaceBbox] Smoothed bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    faceBbox = padBbox(video, faceBbox, paddingPercent)
    print("[getFaceBbox] Padded (H:%2.2f%%, W:%2.2f%%) bbox avg ratios (H, W): %s" % \
        (paddingPercent[0], paddingPercent[1], str(vidBboxAvgRatio(faceBbox))))
    faceBbox = np.array([squareBbox(faceBbox[i]) for i in range(len(faceBbox))])
    print("[getFaceBbox] Squared bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    return np.int32(faceBbox)

def main():
    args = getArgs()

    video = video_read(args.video_path, vid_lib="pims")
    print(f"[main] Read video: {args.video_path}. Length: {len(video)}. FPS: {video.fps:.2f}.")
    print(f"[main] Export images: {args.export_images}. Export video: {args.export_video}")

    print("[main] Extracting face bbox.")
    face_bbox = getFaceBbox(video, args.bbox_padding_percent)
    print(f"[main] Face bbox shape: {face_bbox.shape}")

    print("[main] Extracting face keypoints.")
    face_kps = vidGetFaceKeypoints(video, faceLib="face_alignment")
    print(f"[main] Face keypoints shape: {face_kps.shape}")

    print(f"[main] Exporting to {args.exportDir}.")
    do_export(video, face_bbox, face_kps, args.export_dir, args.export_images, args.export_video)
    print("[main] Done.")

if __name__ == "__main__":
    main()
