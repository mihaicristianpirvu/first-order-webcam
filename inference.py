import numpy as np
import torch as tr
from utils import fTry
from nwutils.kps import rescaleKps
from nwutils.bbox import squareBbox, padBbox, TimeSmoothBbox
from vke.face import imgGetFaceBbox, imgGetFaceKeypoints
from media_processing_lib.image import image_resize
from scipy.spatial import ConvexHull

# Updates the keypoints relative to first frame of a source kp detector w.r.t first frame of driving and current
#  frame of driving. This is because training is done using same video, while testing is done using pairs of video +
#  differnet source image.
def normalize_kp(kp_source, kp_driving, kp_driving_initial, adapt_movement_scale=False,
                 use_relative_movement=False, use_relative_jacobian=False):
    if adapt_movement_scale:
        source_area = ConvexHull(kp_source["value"][0]).volume
        driving_area = ConvexHull(kp_driving_initial["value"][0]).volume
        adapt_movement_scale = np.sqrt(source_area) / np.sqrt(driving_area)
    else:
        adapt_movement_scale = 1

    kp_new = {k: v for k, v in kp_driving.items()}

    if use_relative_movement:
        kp_value_diff = (kp_driving["value"] - kp_driving_initial["value"])
        kp_value_diff *= adapt_movement_scale
        kp_new["value"] = kp_value_diff + kp_source["value"]

        if use_relative_jacobian:
            jacobian_diff = np.matmul(kp_driving["jacobian"], np.linalg.inv(kp_driving_initial["jacobian"]))
            kp_new["jacobian"] = np.matmul(jacobian_diff, kp_source["jacobian"])

    return kp_new

def fKP(model, img):
	assert img.dtype == np.uint8 and len(img.shape) == 3 and img.shape[0 : 2] == (256, 256)
	device = next(model.kp_detector.parameters()).device
	np_img = np.float32(img).transpose(2, 0, 1)[None] / 255
	tr_img = tr.from_numpy(np_img).to(device)
	with tr.no_grad():
		tr_res = model.kp_detector.forward(tr_img)
	np_res = {k: tr_res[k].cpu().numpy() for k in tr_res}
	return np_res

def cropCapture(image, padding, getFaceKps:bool):
	H, W = image.shape[0], image.shape[1]
	res = {"originalImage" : image}
	bbox = imgGetFaceBbox(image, "face_alignment")
	squaredBbox = squareBbox(bbox)
	paddedBbox = padBbox(squaredBbox, padding)
	paddedBbox = np.clip(paddedBbox, 0, [W, W, H, H])
	timeSmoothedBbox = TimeSmoothBbox(t=10)(paddedBbox)
	res["bbox"] = timeSmoothedBbox

	x1, x2, y1, y2 = timeSmoothedBbox
	res["bboxImage"] = np.array(image[y1:y2, x1:x2])
	if getFaceKps:
		faceKpsImage = image_resize(image, height=128, width=None) if image.shape[0] > 128 else image
		faceKps = imgGetFaceKeypoints(faceKpsImage, "face_alignment")
		faceKps = rescaleKps(faceKps, faceKpsImage.shape[0 : 2], image.shape[0 : 2])
		res["originalFaceKps"] = faceKps
		faceKps = faceKps - [x1, y1]
		faceKps = np.clip(faceKps, (0, 0), (x2 - x1 - 1, y2 - y1 - 1))
		res["bboxFaceKps"] = faceKps
	return res

def resizeAndRescale(res, height, width):
	origShape = res["bboxImage"].shape
	res["bboxImage"] = image_resize(res["bboxImage"], mode="black_bars", \
		height=height, width=width, interpolation="lanczos")
	if "bboxFaceKps" in res:
		res["bboxFaceKps"] = rescaleKps(res["bboxFaceKps"], origShape[0 : 2], (height, width))
	return res

def fFrame(model, img, kpSource, kpDrivingNorm):
	assert img.dtype == np.uint8 and len(img.shape) == 3 and img.shape[0 : 2] == (256, 256)
	np_img = np.float32(img).transpose(2, 0, 1)[None] / 255
	device = next(model.generator.parameters()).device
	tr_img = tr.from_numpy(np_img).to(device)
	with tr.no_grad():
		tr_kp_source = {k: tr.from_numpy(kpSource[k]).to(device) for k in kpSource}
		tr_kp_driving = {k: tr.from_numpy(kpDrivingNorm[k]).to(device) for k in kpDrivingNorm}
		tr_res = model.generator.forward(tr_img, kp_source=tr_kp_source, kp_driving=tr_kp_driving)["prediction"][0]
	np_res = tr_res.cpu().numpy().transpose(1, 2, 0)
	np_res = np.uint8(np_res * 255)
	return np_res

def readCropAndResize(g, padding, getFaceKps:bool=False):
	image = next(g)
	res = cropCapture(image, padding, getFaceKps)
	res = resizeAndRescale(res, height=256, width=256)
	return res

# @param[in] model The Model used for keypoint generation and new image generation based on source/driving
# @param[in] g A generator that yields new driving images at each step
# @param[in] sourceImageItems The source image which is used for warping. A dictionary with keys:
#  - "image" : The original image used for warping
#  - "padding" : The automatically detected padding that is used for properly padding/scaling the driving frames
#  - "modelKps" : The keypoints detected by the model. Those are used in combination with driving frames kps to generate
#    a new version of "image" via the movement.
def inference(model, g, sourceImageItems, baseDrivingImageItems, getFaceKps):
	# drivingImageItems - a dictionary to generate motion of sourceImage. Same keys as 'baseDrivingImageItems'.
	drivingImageItems = fTry(readCropAndResize, g, sourceImageItems["padding"], getFaceKps=getFaceKps)
	drivingImageItems["modelKps"] = fKP(model, drivingImageItems["bboxImage"])

	kpDrivingNorm = normalize_kp(kp_source=sourceImageItems["modelKps"], kp_driving=drivingImageItems["modelKps"],
		kp_driving_initial=baseDrivingImageItems["modelKps"], use_relative_movement=True,
		use_relative_jacobian=True, adapt_movement_scale=True)
	outFrame = fFrame(model, sourceImageItems["image"], sourceImageItems["modelKps"], kpDrivingNorm)

	return drivingImageItems, outFrame