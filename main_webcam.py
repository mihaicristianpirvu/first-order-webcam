import cv2
from argparse import ArgumentParser
from pathlib import Path
from media_processing_lib.image import image_read, image_resize
from datetime import datetime

from inference import fKP, readCropAndResize
from utils import fTry, getModel, getPadding
from main_video import get_next_frame

def get(cap):
	while True:
		ret, readFrame = cap.read()
		if ret == False:
			continue	
		origFrame = readFrame[:, 80:-80]
		origFrame = cv2.flip(origFrame, 1)
		origFrame = origFrame[..., ::-1]
		yield origFrame
	raise StopIteration

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--sourceImage", required=True)
	parser.add_argument("--modelCheckpoint", required=True)
	parser.add_argument("--modelConfig", required=True)
	parser.add_argument("--predictFaceKps", type=int, default=0, help="Whether to also predict face keypoints."
		" Useful for debugging, but adds lag.")
	parser.add_argument("--shown_resolution", default="400,400")
	args = parser.parse_args()
	args.sourceImage = Path(args.sourceImage).absolute()
	args.modelCheckpoint = Path(args.modelCheckpoint).absolute()
	args.predictFaceKps = bool(args.predictFaceKps)
	args.modelConfig = Path(args.modelConfig).absolute()
	args.shown_resolution = [int(x) for x in args.shown_resolution.split(",")]
	assert len(args.shown_resolution) == 2
	return args

def main():
	args = getArgs()
	height, width = args.shown_resolution
	model = getModel(args.modelConfig, args.modelCheckpoint)

	# Source image stuff. This is done only once since the source image never changes.
	# Read the image, automatically get padding for the driving video and get the image keypoints using the model.
	sourceImage = image_read(args.sourceImage)
	assert sourceImage.shape[0 : 2] == (256, 256)
	sourceImageItems = {"image" : sourceImage, "padding" : getPadding(sourceImage), \
		"modelKps" : fKP(model, sourceImage), "frame": image_resize(sourceImage, height=height, width=width)}

	g = get(cv2.VideoCapture(0))

	# baseDrivingImageItems - a dictionary used for keypoint alignment between this source of data and the original
	#  sourceImage distribution of data/keypoints. Press 'a' to reset this.
	# Keys:
	#  - "originalImage" - original frame from the camera
	#  - "bboxImage" - cropped around the version of the image
	#  - "modelKps" - detected keypoints around the bboxImage
	#  - "originalFaceKps" (optional) - Face keypoints on the original image (using face_alignment)
	#  - "bboxFaceKps" (optional) - properly rescaled face keypoints on the bbox image (from "originalFaceKps")
	baseDrivingImageItems = fTry(readCropAndResize, g, sourceImageItems["padding"], getFaceKps=args.predictFaceKps)
	baseDrivingImageItems["modelKps"] = fKP(model, baseDrivingImageItems["bboxImage"])

	fps = 0
	while True:
		now = datetime.now()
		frame, drivingImageItems = \
			get_next_frame(model, g, sourceImageItems, baseDrivingImageItems, height, width, args.predictFaceKps)
		fps = (fps * 9 + 1 / (datetime.now() - now).total_seconds()) / 10
		frame = cv2.putText(frame, f"FPS: {fps:2f}", (0, 50), \
			cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0), 1)
		cv2.imshow("Camming is fun", frame)
		if cv2.waitKey(10) & 0xFF == ord("a"):
			baseDrivingImageItems = drivingImageItems

		if cv2.waitKey(10) & 0xFF == ord("q"):
			break

if __name__ == "__main__":
	main()