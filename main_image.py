import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from media_processing_lib.image import image_read, image_resize
from pathlib import Path

from inference import inference, fKP, readCropAndResize
from utils import fTry, presentDrivingFrame, getModel, getPadding

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--sourceImage", required=True)
	parser.add_argument("--drivingImage", required=True)
	parser.add_argument("--modelCheckpoint", required=True)
	parser.add_argument("--modelConfig", required=True)
	args = parser.parse_args()
	args.sourceImage = Path(args.sourceImage).absolute()
	args.modelCheckpoint = Path(args.modelCheckpoint).absolute()
	args.drivingImage = Path(args.drivingImage).absolute()
	args.modelConfig = Path(args.modelConfig).absolute()
	return args

def gen(x):
	while True:
		yield x

def main():
	args = getArgs()

	model = getModel(args.modelConfig, args.modelCheckpoint)

	# Source image stuff. This is done only once since the source image never changes.
	# Read the image, automatically get padding for the driving video and get the image keypoints using the model.
	sourceImage = image_read(args.sourceImage)
	assert sourceImage.shape[0 : 2] == (256, 256)
	sourceImageItems = {"image" : sourceImage, "padding" : getPadding(sourceImage), \
		"modelKps" : fKP(model, sourceImage)}
	sourceFrame = image_resize(sourceImage, height=500, width=500)

	g = gen(image_read(args.drivingImage))

	# baseDrivingImageItems - a dictionary used for keypoint alignment between this source of data and the original
	#  sourceImage distribution of data/keypoints. Press 'a' to reset this.
	# Keys:
	#  - "originalImage" - original frame from the camera
	#  - "bboxImage" - cropped around the version of the image
	#  - "modelKps" - detected keypoints around the bboxImage
	#  - "originalFaceKps" (optional) - Face keypoints on the original image (using face_alignment)
	#  - "bboxFaceKps" (optional) - properly rescaled face keypoints on the bbox image (from "originalFaceKps")
	baseDrivingImageItems = fTry(readCropAndResize, g, sourceImageItems["padding"], getFaceKps=True)
	baseDrivingImageItems["modelKps"] = fKP(model, baseDrivingImageItems["bboxImage"])

	while True:
		drivingImageItems, outFrame = inference(model, g, sourceImageItems, baseDrivingImageItems, getFaceKps=True)
		bboxDrivingFrame = presentDrivingFrame(drivingImageItems)
		originalDrivingFrame = image_resize(drivingImageItems["originalImage"], \
			height=500, width=500, interpolation="lanczos", mode="black_bars")
		outFrame = image_resize(outFrame, height=500, width=500, interpolation="lanczos")

		image = np.concatenate([originalDrivingFrame, sourceFrame, bboxDrivingFrame, outFrame], axis=1)
		plt.gcf().clf()
		plt.title("Original Driving. Original Source. Cropped driving. Generated.")
		plt.imshow(image)
		plt.show()

if __name__ == "__main__":
	main()