import cv2
import numpy as np
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from media_processing_lib.image import image_resize, image_read
from media_processing_lib.video import MPLVideo, video_read
from nwutils.bbox import frameRectangler

from inference import inference, fKP, readCropAndResize
from utils import fTry, presentDrivingFrame, getModel, getPadding

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("--sourceImage", required=True)
	parser.add_argument("--drivingVideo", required=True)
	parser.add_argument("--modelCheckpoint", required=True)
	parser.add_argument("--modelConfig", required=True)
	parser.add_argument("--shown_resolution", default="500,500")
	args = parser.parse_args()
	args.sourceImage = Path(args.sourceImage).absolute()
	args.modelCheckpoint = Path(args.modelCheckpoint).absolute()
	args.modelConfig = Path(args.modelConfig).absolute()
	args.drivingVideo = Path(args.drivingVideo).absolute()
	args.shown_resolution = [int(x) for x in args.shown_resolution.split(",")]
	assert len(args.shown_resolution) == 2
	return args

def videoGenerator(video: MPLVideo) -> np.ndarray:
	i = 0
	while True:
		item = video[i % len(video)]
		print(f"[videoGenerator] Frame: {i}. Shape: {item.shape}")
		yield item
		i += 1


def get_next_frame(model, g, sourceImageItems, baseDrivingImageItems, height, width, getFaceKps: bool) -> np.ndarray:
	drivingImageItems, outFrame = inference(model, g, sourceImageItems, baseDrivingImageItems, getFaceKps=getFaceKps)
	bboxDrivingFrame = presentDrivingFrame(drivingImageItems, height, width)
	originalDrivingFrame = image_resize(frameRectangler(drivingImageItems["originalImage"], \
		drivingImageItems["bbox"]), height=height, width=width, interpolation="lanczos", mode="black_bars")
	outFrame = image_resize(outFrame, height=height, width=width, interpolation="lanczos")

	image = np.concatenate([originalDrivingFrame, sourceImageItems["frame"], bboxDrivingFrame, outFrame], axis=1)
	# BGR to RGB
	frame = image[..., ::-1].copy()
	return frame, drivingImageItems

def main():
	args = getArgs()
	model = getModel(args.modelConfig, args.modelCheckpoint)
	height, width = args.shown_resolution
	# cv2.startWindowThread()

	# Source image stuff. This is done only once since the source image never changes.
	# Read the image, automatically get padding for the driving video and get the image keypoints using the model.
	sourceImage = image_read(args.sourceImage)
	assert sourceImage.shape[0 : 2] == (256, 256)
	sourceImageItems = {"image" : sourceImage, "padding" : getPadding(sourceImage), \
		"modelKps" : fKP(model, sourceImage), "frame": image_resize(sourceImage, height=height, width=width)}

	g = videoGenerator(video_read(args.drivingVideo, vid_lib="pims"))

	# baseDrivingImageItems - a dictionary used for keypoint alignment between this source of data and the original
	#  sourceImage distribution of data/keypoints. Press 'a' to reset this.
	# Keys:
	#  - "originalImage" - original frame from the camera
	#  - "bboxImage" - cropped around the version of the image
	#  - "modelKps" - detected keypoints around the bboxImage
	#  - "originalFaceKps" (optional) - Face keypoints on the original image (using face_alignment)
	#  - "bboxFaceKps" (optional) - properly rescaled face keypoints on the bbox image (from "originalFaceKps")
	baseDrivingImageItems = fTry(readCropAndResize, g, sourceImageItems["padding"], getFaceKps=False)
	baseDrivingImageItems["modelKps"] = fKP(model, baseDrivingImageItems["bboxImage"])

	while True:
		frame, _ = get_next_frame(model, g, sourceImageItems, baseDrivingImageItems, height, width, getFaceKps=False)
		cv2.imshow("Hi?", frame)
		if cv2.waitKey(10) & 0xFF == ord("q"):
			break
		if cv2.waitKey(10) & 0xFF != ord("a"):
			cv2.waitKey(0)

if __name__ == "__main__":
	main()
